




all:
	g++  -Iinc/ src/diffusion.cpp src/diffunc.cpp -o diffusion `root-config --cflags --glibs`


clean:
	rm diffusion
	rm log
