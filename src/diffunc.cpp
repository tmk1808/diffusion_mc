#include "Rootlibs.h"
#include <iostream>
#include "diffunc.h"
#include <fstream>
#include <vector>
#include <string>

void fillSigma(std::string infile, std::vector<double>&  x, std::vector<double>&  y) {
ifstream in;
in.open(infile.c_str());

double mev = 0.0;
double barn = 0.0;
int nlines =0;

while (1) {
in >> mev >> barn;
x.push_back(mev); y.push_back(barn);
if(!in.good()) break;
nlines++;

}
in.close();
std::cout << infile << " has been filled into the vectors" << std::endl;

}

double Sigma(double energ, std::vector<double>& x, std::vector<double>& y) {

if (energ < x[0]) return y[0];
else if (energ > x[x.size()-1]) return y[y.size()-1];
for (int i = 1;i< x.size()-1; i++) {

if ( energ < x[i+1] && energ > x[i-1]) return y[i];

}
return 0;
}








double calc_w(double a, double randnum1, double randnum2, double randnum3) {

  double w = a * (-TMath::Log(randnum1) - TMath::Log(randnum2) * cos(TMath::Pi() * randnum3 / 2.0) * cos(TMath::Pi() * randnum3 / 2.0) );

  return w;


}


double wattfunc(double a, double b,double randnum, double w){


double ene = w + (a*a*b)/ 4.0 + (2.0 * randnum - 1.0) * sqrt(a*a*b*w);

return ene;
}

double interaction_point(double l, double randnum ){

  double length =  l * TMath::Log(randnum);

  return length;

}

double interaction_radius(double lamb, double randnum) {

  return - lamb * TMath::Log(randnum * lamb);
}
