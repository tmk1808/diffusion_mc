#include "Rootlibs.h"
#include <iostream>
#include <vector>
#include "diffunc.h"
using namespace std;
/////////////////////////////////////////////////////////
//
//
//
//
//
//
////////////////////////////////////////////////////////
void mcdiffusion(int,bool);

std::vector<double> h1_n_el_mev;
std::vector<double> o16_n_el_mev;
std::vector<double> u235_n_el_mev;
std::vector<double> u238_n_el_mev;

std::vector<double> h1_n_el_sig;
std::vector<double> o16_n_el_sig;
std::vector<double> u235_n_el_sig;
std::vector<double> u238_n_el_sig;
//////////////////////////////////////
std::vector<double> h1_n_g_mev;
std::vector<double> o16_n_g_mev;
std::vector<double> u235_n_g_mev;
std::vector<double> u238_n_g_mev;

std::vector<double> h1_n_g_sig;
std::vector<double> o16_n_g_sig;
std::vector<double> u235_n_g_sig;
std::vector<double> u238_n_g_sig;
//////////////////////////////////////
std::vector<double> u235_n_f_mev;
std::vector<double> u238_n_f_mev;

std::vector<double> u235_n_f_sig;
std::vector<double> u238_n_f_sig;


//////////////////////////////////////
TRandom3 * rand1 = new TRandom3(0);
TRandom3 * rand2 = new TRandom3(0);
TRandom3 * rand3 = new TRandom3(0);
TRandom3 * rand4 = new TRandom3(0);
TRandom3 * rand5 = new TRandom3(0);
TRandom3 * rand6 = new TRandom3(0);
TRandom3 * rand7 = new TRandom3(0);
TRandom3 * rand8 = new TRandom3(0);
TRandom3 * rand9 = new TRandom3(0);
TRandom3 * rand10 = new TRandom3(0);
TRandom3 * rand11 = new TRandom3(0);
TRandom3 * rand12 = new TRandom3(0);
TRandom3 * rand13 = new TRandom3(0);
TRandom3 * rand14 = new TRandom3(0);

//////////////////////////////////////
int bankneutrons = 0;
int outofbounds = 0;
double Sum_d = 0.0;
double volume = TMath::Pi() * 5.0 * 5.0;
double surface = 2.0 * TMath::Pi() *5 + 2.0 * TMath::Pi() * TMath::Sqrt(5) * 5; 
std::vector<double> bankx;
std::vector<double> banky;
std::vector<double> bankz;


int scattered_h = 0;
int scattered_o = 0;
int scattered_5 = 0;
int scattered_8 = 0;

int capture_h = 0;
int capture_o = 0;
int capture_5 = 0;
int capture_8 = 0;

int fission_5 = 0;
int fission_8 = 0;

int main(int argc, char **argv) {

    TApplication theApp("App", &argc, argv);
    int startn = 100000;
    fillSigma("input/H1-N-EL.DAT",h1_n_el_mev, h1_n_el_sig);
    fillSigma("input/O16-N-EL.DAT",o16_n_el_mev, o16_n_el_sig);
    fillSigma("input/U235-N-EL.DAT",u235_n_el_mev, u235_n_el_sig);
    fillSigma("input/U238-N-EL.DAT",u238_n_el_mev, u238_n_el_sig);

    fillSigma("input/H1-N-G.DAT",h1_n_g_mev, h1_n_g_sig);
    fillSigma("input/O16-N-G.DAT",o16_n_g_mev, o16_n_g_sig);
    fillSigma("input/U235-N-G.DAT",u235_n_g_mev, u235_n_g_sig);
    fillSigma("input/U238-N-G.DAT",u238_n_g_mev, u238_n_g_sig);


    fillSigma("input/U235-N-F.DAT",u235_n_f_mev, u235_n_f_sig);
    fillSigma("input/U238-N-F.DAT",u238_n_f_mev, u238_n_f_sig);

//    cout << Sigma(19.5,h1_n_el_mev, h1_n_el_sig);
    mcdiffusion(startn,false);
    cout << endl << endl << endl << "NOW BANK WITH " << bankneutrons << " NEUTRONS" << endl;
    mcdiffusion(bankneutrons,true); 
    cout << "Current: " << 1.0 / ( (startn + bankneutrons) * surface )  * outofbounds << endl;
    cout << "Flux: " << 1.0 / ( ( startn + bankneutrons) * volume ) * Sum_d << endl;
    //theApp.Run();
    cout << scattered_h << " neutrons have been scattered off H" << endl;
    cout << scattered_o << " neutrons have been scattered off O" << endl;
    cout << scattered_5 << " neutrons have been scattered off U235" << endl;
    cout << scattered_8 << " neutrons have been scattered off U238" << endl;

    cout << capture_h << " neutrons have been captured by H" << endl;
    cout << capture_o << " neutrons have been captured by O" << endl;
    cout << capture_5 << " neutrons have been captured by U235" << endl;
    cout << capture_8 << " neutrons have been captured by U238" << endl;


    cout << fission_5 << " neutrons produced fission on U235" << endl;
    cout << fission_8 << " neutrons produced fission on U238" << endl;

    return 0;


}


void mcdiffusion(int Nsample = 1000, bool bb = false){
bool DEBUG = false;


int captured = 0;
double rn = 0.0;
double rn2 = 0.0;

double A = 0.0;
double myu_cm = 0.0;
double myu_lab = 0.0;

char type = 'a';
const double a = 1.0 / 1.036; //watt function parameter in MeV
const double b = 2.29; 
double x = 0.0;
double y = 0.0;
double z = 0.0;
double u = 0.0;
double v = 0.0;
double w = 0.0;
double u_p = 0.0;
double v_p = 0.0;
double w_p = 0.0;
double R = 0.0;
double phi = 0.0;


double Nh = 0.037;
double No = 0.0314;
double Nu5 = 0.000236;;
double Nu8 = 0.00623;

double Sigma_All = 0.0;
double Sigma_H_T = 0.0;
double Sigma_O_T = 0.0;
double Sigma_U5_T = 0.0;
double Sigma_U8_T = 0.0;

double sigma_H_S = 0.0;
double sigma_H_C = 0.0;
double sigma_H_F = 0.0;
double sigma_H_T = 0.0;

double sigma_O_S = 0.0;
double sigma_O_C = 0.0;
double sigma_O_F = 0.0;
double sigma_O_T = 0.0;

double sigma_U5_S = 0.0;
double sigma_U5_C = 0.0;
double sigma_U5_F = 0.0;
double sigma_U5_T = 0.0;


double sigma_U8_S = 0.0;
double sigma_U8_C = 0.0;
double sigma_U8_F = 0.0;
double sigma_U8_T = 0.0;

double p1 = 0.0;
double p2 = 0.0;
double p3 = 0.0;
double p4 = 0.0;

double intSigmaS = 0.0;
double intSigmaC = 0.0;
double intSigmaF = 0.0;
double intSigmaT = 0.0;

// declaring random numbers

double ene = 0.0;
for (int i = 0; i < Nsample;i++){
    //generate neutron and interaction coordinates
    if (bb) {
    x = bankx[i];
    y = banky[i];
    z = bankz[i];
    }
    else {
    x = rand1->Uniform(0,1);
    y = rand2->Uniform(0,1);
    z = rand3->Uniform(0,1);
    }
    phi = 2 * TMath::Pi() * rand4->Uniform(0,1);
    u = 2.0 * rand5->Uniform(0,1) - 1.0;
    v = TMath::Sqrt(1-u*u) * cos(phi);
    w = TMath::Sqrt(1-u*u) * sin(phi);

    // Determinig neutron energy
    ene = wattfunc(a,b,rand6->Uniform(0,1), calc_w(a,rand7->Uniform(0,1),rand8->Uniform(0,1),rand9->Uniform(0,1)));


CalcSig:

    sigma_H_S = Sigma(ene, h1_n_el_mev, h1_n_el_sig);
    sigma_H_C = Sigma(ene, h1_n_g_mev, h1_n_g_sig);

    sigma_H_T = sigma_H_S + sigma_H_C + sigma_H_F;
    Sigma_H_T = Nh * sigma_H_T;

    sigma_O_S = Sigma(ene, o16_n_el_mev, o16_n_el_sig);
    sigma_O_C = Sigma(ene, o16_n_g_mev, o16_n_g_sig);

    sigma_O_T = sigma_O_S + sigma_O_C + sigma_O_F;
    Sigma_O_T = No * sigma_O_T;

    sigma_U5_S = Sigma(ene, u235_n_el_mev, u235_n_el_sig);
    sigma_U5_C = Sigma(ene, u235_n_g_mev, u235_n_g_sig);
    sigma_U5_F = Sigma(ene, u235_n_f_mev, u235_n_f_sig);

    sigma_U5_T = sigma_U5_S + sigma_U5_C + sigma_U5_F;
    Sigma_U5_T = Nu5 * sigma_U5_T;

    sigma_U8_S = Sigma(ene, u238_n_el_mev, u238_n_el_sig);
    sigma_U8_C = Sigma(ene, u238_n_g_mev, u238_n_g_sig);
    sigma_U8_F = Sigma(ene, u238_n_f_mev, u238_n_f_sig);

    sigma_U8_T = sigma_U8_S + sigma_U8_C + sigma_U8_F;
    Sigma_U8_T = Nu8 * sigma_U8_T;

    Sigma_All = Sigma_H_T + Sigma_O_T + Sigma_U5_T + Sigma_U8_T;

    p1 = Sigma_H_T / Sigma_All;
    p2 = Sigma_O_T / Sigma_All;
    p3 = Sigma_U5_T / Sigma_All;
    p4 = Sigma_U8_T / Sigma_All;


    R = - TMath::Log(1 - rand10->Uniform(0,1)) / Sigma_All; 

    x = x + R * u;
    y = y + R * v;
    z = z + R * w;

    if ( x*x + y*y > 5 || z > 5) { outofbounds++; cout << "Out of bounds" << endl; continue;}
    
    Sum_d += R;
    rn = rand11->Uniform(0,1);

    if (rn <= p1) { 
                                      intSigmaS = sigma_H_S; 
                                      intSigmaC = sigma_H_C; 
                                      intSigmaF = sigma_H_F; 
                                      intSigmaT = sigma_H_T; 
                                      A = 1.0;
                                      type = 'h';
                                    }

    else if (rn > p1 && rn <= p1+p2 ) { 
                                      intSigmaS = sigma_O_S; 
                                      intSigmaC = sigma_O_C; 
                                      intSigmaF = sigma_O_F; 
                                      intSigmaT = sigma_O_T; 
                                      A = 16.0;
                                      type = 'o';
                                    }

    else if (rn > p1+p2 && rn <= p1+p2+p3 ) { 
                                      intSigmaS = sigma_U5_S; 
                                      intSigmaC = sigma_U5_C; 
                                      intSigmaF = sigma_U5_F; 
                                      intSigmaT = sigma_U5_T; 
                                      A = 235.0;
                                      type = '5';
                                    }
    else if (rn > p1+p2+p3 && rn <= p1+p2+p3+p4 ) { 
                                      intSigmaS = sigma_U8_S; 
                                      intSigmaC = sigma_U8_C; 
                                      intSigmaF = sigma_U8_F; 
                                      intSigmaT = sigma_U8_T; 
                                      A = 238.0;
                                      type = '8';
                                    }
    p1 = intSigmaS / intSigmaT;
    p2 = intSigmaC / intSigmaT;
    p3 = intSigmaF / intSigmaT;
    rn2 = rand12->Uniform(0,1);
    
    if (rn2 <= p1 ) {
       myu_cm = rand13->Uniform(-1,1);
       ene = ene * (A*A + 2.0 * A * myu_cm +1) / ( (A+1)*(A+1) );
       myu_lab = (1 + A * myu_cm) / TMath::Sqrt(A*A + 2.0 * A * myu_cm + 1);
       phi = 2 * TMath::Pi() * rand14->Uniform(0,1);
       
       u_p = myu_lab * u + (TMath::Sqrt(1.0 - myu_lab * myu_lab) * ( u * w * cos(phi) - v * sin(phi) ) ) / TMath::Sqrt(1 - w * w); 
       v_p = myu_lab * v + (TMath::Sqrt(1.0 - myu_lab * myu_lab) * ( v * w * cos(phi) + u * sin(phi) ) ) / TMath::Sqrt(1 - w * w); 
       w_p = myu_lab * w - TMath::Sqrt(1.0 - myu_lab * myu_lab) * TMath::Sqrt(1.0 - w * w) * cos(phi);


       u = u_p;
       v = v_p;
       w = w_p;
       cout << "SCATTERED" <<endl;
       cout << "New Energy: " << ene << " MeV" << endl;

       if (type == 'h') scattered_h++;
       if (type == 'o') scattered_o++;
       if (type == '5') scattered_5++;
       if (type == '8') scattered_8++;
       goto CalcSig;


} 
    else if (rn2 > p1 && rn2 <= p1+p2) {
           captured++;
           
           if (type == 'h') capture_h++;
           if (type == 'o') capture_o++;
           if (type == '5') capture_5++;
           if (type == '8') capture_8++;


          }
    else if (rn2 > p1+p2 && rn2 <= p1+p2+p3) { 
          bankneutrons+=2; 
          for (int j = 0;j<2;j++) 
              {bankx.push_back(x);banky.push_back(y);bankz.push_back(z);}
          if (bb) Nsample+=2;
      

          if (type == '5') fission_5++;
          if (type == '8') fission_8++;
                                             } 

    Printf("X\t Y\t Z\t Energy\t R\t Angle\t");
    Printf("%1.2f\t %1.2f\t %1.2f\t %1.2f\t %1.2f\t %1.2f\t %1.2f\t %i\t %c\t",x,y,z,ene,R,phi,intSigmaF,bankneutrons, type);
      


 }



 cout << outofbounds << " Neutrons escaped the volume" << endl;
 cout << captured << " Neutrons captured" << endl;
}
