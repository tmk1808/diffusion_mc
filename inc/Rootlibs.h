#ifndef Rootlibs_H
#define Rootlibs_H


#include <TROOT.h>
#include <TApplication.h>
#include <TStyle.h>
#include <TRandom3.h>
#include <TH1.h>
#include <TLegend.h>
#include <TText.h>
#include <TVirtualX.h>
#include <TGWindow.h>
#include <TGFrame.h>
#include <TGClient.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGListBox.h>
#include <TGNumberEntry.h>
#include <TFrame.h>
#include <TRint.h>
#include <TThread.h>
#include <TEnv.h>
#include <TGraph.h>
#include <TGLabel.h>

#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGStatusBar.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TGMsgBox.h>

#endif
