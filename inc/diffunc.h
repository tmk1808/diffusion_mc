#ifndef DIFFUNC_H
#define DIFFUNC_H

#include <vector>
////////////////////////////////////////////
extern std::vector<double> h1_n_el_mev;
extern std::vector<double> o16_n_el_mev;
extern std::vector<double> u235_n_el_mev;
extern std::vector<double> u238_n_el_mev;

extern std::vector<double> h1_n_el_sig;
extern std::vector<double> o16_n_el_sig;
extern std::vector<double> u235_n_el_sig;
extern std::vector<double> u238_n_el_sig;
/////////////////////////////////////////////
extern std::vector<double> h1_n_g_mev;
extern std::vector<double> o16_n_g_mev;
extern std::vector<double> u235_n_g_mev;
extern std::vector<double> u238_n_g_mev;

extern std::vector<double> h1_n_g_sig;
extern std::vector<double> o16_n_g_sig;
extern std::vector<double> u235_n_g_sig;
extern std::vector<double> u238_n_g_sig;
/////////////////////////////////////////////
extern std::vector<double> u235_n_f_mev;
extern std::vector<double> u238_n_f_mev;

extern std::vector<double> u235_n_f_sig;
extern std::vector<double> u238_n_f_sig;


void fillSigma(std::string, std::vector<double>&, std::vector<double>&);
double Sigma(double energy, std::vector<double>&, std::vector<double>&);
//double cross(double);
double calc_w(double, double, double,double);
double wattfunc(double,double,double,double);
double interaction_point(double , double);
double interaction_radius(double,double);




#endif




